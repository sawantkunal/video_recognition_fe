import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoginRoutingModule } from './login.routing.module';
import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterceptorModule } from 'src/app/helpers/interceptor.module';
import { HeaderModule } from 'src/app/components/header/header.module';
import { CamerasModule } from 'src/app/modules/cameras/cameras.module';

@NgModule({
    // [...]
    declarations: [LoginComponent],
    imports: [
        CommonModule,
        FormsModule,
        InterceptorModule,
        LoginRoutingModule,
        ReactiveFormsModule,
        HeaderModule,
        CamerasModule
    ],
    exports: [
        LoginComponent
    ],
    providers: [LoginService]
})
export class LoginModule { }
