import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }
  loginUser(loginData): any {
    return this.http.post('https://run.mocky.io/v3/24bcf976-a1a7-41f1-8c6f-a7dadf58a25c', loginData, { observe: 'response' })
    .pipe(map(res => {
        if (res.status == 200) {
          return res;
        }
    }));
  }
}
