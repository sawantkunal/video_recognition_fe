import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  closeModal = false;
  submitted: boolean;
  userData: any;
  next = 'form-0';


  constructor(private router: Router, private fb: FormBuilder, private loginService: LoginService) { }

  ngOnInit(): void {
    this.userData = JSON.parse(localStorage.getItem('auth'));
    if (this.userData != null) {
      this.router.navigate(['/cameras']);
    }
    this.loginReactiveForm();
  }

  loginReactiveForm(): any {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.email, Validators.required, Validators.pattern('^[a-z][a-zA-Z0-9.-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{1,}$')])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(5)])]
    });
  }

  get logForm(): any { return this.loginForm.controls; }
  loginSubmit(): any {
    this.submitted = true;
    if (this.loginForm.valid) {
      this.loginService.loginUser(this.loginForm.value)
        .subscribe((res: any) => {
          if (res.status === 200) {
            localStorage.setItem('auth', JSON.stringify(res));
            this.submitted = false;
            this.router.navigate(['/cameras']);
          } else {
            localStorage.removeItem('auth');
          }
        });
    }
  }
}
