import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  {
    path: 'login', loadChildren: () => import('./authentications/login/login.module').then(m => {
      return m.LoginModule;
    })
  },
  {
    path: 'cameras', canActivate: [AuthService], loadChildren: () => import('./modules/cameras/cameras.module').then(m => {
      return m.CamerasModule;
    })
  },
  {
    path: 'reporting', canActivate: [AuthService], loadChildren: () => import('./modules/reporting/reporting.module').then(m => {
      return m.ReportingModule;
    })
  },
  {
    path: 'setting', canActivate: [AuthService], loadChildren: () => import('./modules/setting/setting.module').then(m => {
      return m.SettingModule;
    })
  },
  { path: '**', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
