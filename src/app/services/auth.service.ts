import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private router: Router,
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): any {
    const currentUser = JSON.parse(localStorage.getItem('auth'));
    if (currentUser != null) {
      if (currentUser.body.email) {
        return true;
      }
    }
    this.router.navigate(['/login']);
    return false;
  }
}
