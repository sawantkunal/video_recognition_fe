import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SettingComponent } from './setting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterceptorModule } from '../../helpers/interceptor.module';
import { SettingRoutingModule } from './setting.routing.module';
import { HeaderModule } from '../../components/header/header.module';

@NgModule({
    // [...]
    declarations: [SettingComponent],
    imports: [
        CommonModule,
        FormsModule,
        InterceptorModule,
        ReactiveFormsModule,
        RouterModule,
        SettingRoutingModule,
        HeaderModule
    ],
    exports: [
        SettingComponent
    ],
    providers: []
})
export class SettingModule { }
