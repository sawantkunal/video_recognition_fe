import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CamerasComponent } from './cameras.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterceptorModule } from '../../helpers/interceptor.module';
import { CamerasRoutingModule } from './cameras.routing.module';
import { HeaderModule } from 'src/app/components/header/header.module';
import { CamerasService } from './cameras.service';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@NgModule({
    // [...]
    declarations: [CamerasComponent],
    imports: [
        CommonModule,
        FormsModule,
        InterceptorModule,
        ReactiveFormsModule,
        CamerasRoutingModule,
        RouterModule,
        HeaderModule,
        TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: httpTranslateLoader,
        deps: [HttpClient]
      }
    })
    ],
    exports: [
        CamerasComponent
    ],
    providers: [CamerasService]
})
export class CamerasModule { }

// AOT compilation support
export function httpTranslateLoader(http: HttpClient): any {
    return new TranslateHttpLoader(http);
  }
