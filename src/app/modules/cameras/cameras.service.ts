import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CamerasService {

  constructor(private http: HttpClient) { }
  getCameraDetails(i): any {
    if (i == 0) {
      var url = 'https://run.mocky.io/v3/9cc35051-41cc-427f-8cdf-fc76fc9f8cc4';
      i = i++;
    } else {
      var url = 'https://run.mocky.io/v3/f1ddcdb8-18b2-4881-b226-f79376f75718';
      i = 0;
    }
    return this.http.get(url, { observe: 'response' })
      .pipe(map(res => {
        if (res.status == 200) {
          return res;
        }
      }));
  }
}
