import { Component, OnInit } from '@angular/core';
import { CamerasService } from './cameras.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-cameras',
  templateUrl: './cameras.component.html',
  styleUrls: ['./cameras.component.css']
})
export class CamerasComponent implements OnInit {

  alldDetails: any;
  constructor(private camerasService: CamerasService,
    public translate: TranslateService
  ) {
    translate.addLangs(['en', 'nl']);
    translate.setDefaultLang('en');
  }

  ngOnInit(): void {
    let i = 0;
    setInterval(() => {
      this.getDetails(i);
      if (i == 1) {
        i = 0;
      } else {
        i = 1;
      }
    }, 500);
  }
  switchLang(lang: string): any {
    this.translate.use(lang);
  }
  getDetails(i): any {
    this.camerasService.getCameraDetails(i).
      subscribe(res => {
        if (res.status == 200) {
          this.alldDetails = res.body.data;
        }
      });
  }

}
