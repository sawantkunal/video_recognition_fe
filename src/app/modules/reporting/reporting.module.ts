import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ReportingComponent } from './reporting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InterceptorModule } from '../../helpers/interceptor.module';
import { ReportingRoutingModule } from './reporting.routing.module';
import { HeaderModule } from '../../components/header/header.module';

@NgModule({
    // [...]
    declarations: [ReportingComponent],
    imports: [
        CommonModule,
        FormsModule,
        InterceptorModule,
        ReactiveFormsModule,
        ReportingRoutingModule,
        RouterModule,
        HeaderModule
    ],
    exports: [
        ReportingComponent
    ],
    providers: []
})
export class ReportingModule { }
