import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  url: any;
  constructor(private router: Router, private route: ActivatedRoute,
    public translate: TranslateService
  ) {
    translate.addLangs(['en', 'nl']);
    translate.setDefaultLang('en');
  }


  ngOnInit(): void {
  }
  switchLang(lang: string): any {
    this.translate.use(lang);
  }
  signout(): any {
    localStorage.removeItem('auth');
    this.router.navigate(['/login']);
  }

}
